<?php

class Api_model extends CI_Model {

    function __construct(){
        parent::__construct();
    }


    function fb_exist($fb_id) {
        
        $this->db->where('fb_id', $fb_id);
        if ($this->db->get('tb_user')->num_rows() > 0) {
            
            return 1;
        } else {
            return 0;
        }
    }
    
    function email_exist($email) {
        
        $this->db->where('email', $email);
        if ($this->db->get('tb_user')->num_rows() > 0) {
            
            return 1;
        } else {
            return 0;
        }
    }
    
    function getUserIdByEmail($email) {
        
        return $this->db->where('email', $email)->get('tb_user')->row()->id;
    }

    
    function register($username, $email, $password_hash) {
        
        $this->db->set('username', $username);
        $this->db->set('email', $email);
        $this->db->set('password', $password_hash);
        $this->db->set('reg_date', 'NOW()', false);
        $this->db->insert('tb_user');
        $user_id = $this->db->insert_id();
        
        $this->db->set('user_id', $user_id);
        $this->db->insert('tb_property');
        $this->db->insert_id();
        
        $this->db->set('user_id', $user_id);
        $this->db->insert('tb_macro');
        $this->db->insert_id();
        
        return $user_id;
    }
    
    function getPropoerty($user_id) {
        
        $result = array();
        
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('tb_property');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $result = array('gender' => $row->gender,
                            'birthday' => $row->birthday,
                            'age' => $row->age,
                            'bodyWeight' => $row->body_weight,
                            'height' => $row->height,
                            'activityFactor' => $row->activity_factor,
                            'wristMeasurement' => $row->wrist_measurement,
                            'waistMeasurement' => $row->waist_measurement,
                            'averageHip' => $row->average_hip,
                            'averageAbdomen' => $row->average_abdomen,
                            'startingBodyWeight' => $row->starting_bodyweight,
                            );          
        } else {
            
            $result = array('gender' => '',
                            'birthday' => '',
                            'age' => 0,
                            'bodyWeight' => 0,
                            'height' => 0,
                            'activityFactor' => '',
                            'wristMeasurement' => 0,
                            'waistMeasurement' => 0,
                            'averageHip' => 0,
                            'averageAbdomen' => 0,
                            'startingBodyWeight' => 0
                            ); 
            
        }
        return $result;
    }
    
    function getMacroPoint($user_id) {
        
        $result = array();
        
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('tb_macro');
        if ($query->num_rows() > 0) {            
            
            $row = $query->row();
            
            if(strtotime($row->block_uptodate) != strtotime(('Y-m-d'))) {                  
                $this->resetEatBlocks($user_id);                 
            }
                
            $result = array('bodyFat' => $row->body_fat,
                            'bodyMass' => $row->body_mass,
                            'dailyProtein' => $row->daily_protein,
                            'dailyCarb' => $row->daily_carb,
                            'dailyFat' => $row->daily_fat,
                            'totalBlocks' => $row->total_blocks,
                            'eatBlocksTillNow' => $row->eaten_blocks,
                            'startingBodyFatPercent' => $row->starting_bdoy_fat_percent
            );                
        }

        return $result;  
 
        
    }
    
    function uploadPersonalInfo($user_id, $gender, $birthday, $age, $body_weight, $height, $activity_factor, 
                                $wrist_measurement, $waist_measurement, $average_hip, $average_abdomen, $starting_bodyweight) {
        
        if ( $this->db->where('id', $user_id)->get('tb_user')->num_rows() > 0) {
            if ($this->db->where('user_id', $user_id)->get('tb_property')->num_rows() == 0) {
        
                $this->db->set('user_id', $user_id);
                $this->db->set('gender', $gender);
                $this->db->set('birthday', $birthday);
                $this->db->set('age', $age);
                $this->db->set('body_weight', $body_weight);
                $this->db->set('height', $height);
                $this->db->set('activity_factor', $activity_factor);
                $this->db->set('wrist_measurement', $wrist_measurement);
                $this->db->set('waist_measurement', $waist_measurement);
                $this->db->set('average_hip', $average_hip);
                $this->db->set('average_abdomen', $average_abdomen); 
                $this->db->set('starting_bodyweight', $starting_bodyweight); 
                                
                $this->db->insert('tb_property');
                $this->db->insert_id();
            }else {
                
                $this->db->where('user_id', $user_id);
                
                $this->db->set('gender', $gender);
                $this->db->set('birthday', $birthday);
                $this->db->set('age', $age);
                $this->db->set('body_weight', $body_weight);
                $this->db->set('height', $height);
                $this->db->set('activity_factor', $activity_factor);
                $this->db->set('wrist_measurement', $wrist_measurement);
                $this->db->set('waist_measurement', $waist_measurement);
                $this->db->set('average_hip', $average_hip);
                $this->db->set('average_abdomen', $average_abdomen);
                $this->db->set('starting_bodyweight', $starting_bodyweight);

                $this->db->update('tb_property');            
            }
        } 
    }
    
    function registerWithSocial($username, $email, $password_hash) {
        
        $this->db->set('username', $username);
        $this->db->set('email', $email);
        $this->db->set('password', $password_hash);
        $this->db->set('reg_date', 'NOW()', false);
        $this->db->insert('tb_user');
        $user_id =  $this->db->insert_id();
        
        $this->db->set('user_id', $user_id);
        $this->db->insert('tb_property');
        $this->db->insert_id();
        
        $this->db->set('user_id', $user_id);
        $this->db->insert('tb_macro');
        $this->db->insert_id();
        
        return $user_id;
        
    }
    
    function groupExist($group_name) {
        
        $this->db->where('group_name', $group_name);
        $query =  $this->db->get('tb_group');
        if ($query->num_rows() > 0) {
            
            return $query->row()->id;
        } else {
            return 0;
        }
    }
    
    function createGroup($user_id, $creator_name, $group_name, $kind_group, $group_type, $point_param, $price, $start_date, $end_date) {
        
        $this->db->set('user_id', $user_id);
        $this->db->set('creator_name', $creator_name);
        $this->db->set('group_name', $group_name);
        $this->db->set('kind_group', $kind_group);
        $this->db->set('group_type', $group_type);
        $this->db->set('point_parameter', $point_param);
        $this->db->set('price', $price);
        $this->db->set('start_date', $start_date);
        $this->db->set('end_date', $end_date);
        $this->db->set('reg_date', 'NOW()', false);
        $this->db->insert('tb_group');
        return $this->db->insert_id();
        
        
    }
    
    function getUserInfoByName($username) {
        
        $result = array();
        
        $query = $this->db->select('*')
                  ->from('tb_user')

                  ->where("(username LIKE '%".$username."%' OR email LIKE '%".$username."%')", NULL, FALSE)
                  ->get();
        /*
        $this->db->like('username', $username);
        $this->db->like('email', $username);
        $query = $this->db->get('tb_user');
        */
        if ($query->num_rows() > 0) {
            
            foreach ($query->result() as $user) {
                
                $arr = array('userId' => $user->id,
                             'userName' => $user->username, 
                             'email' => $user->email);
                array_push($result, $arr);
            }                        
        }
        return $result;
    }
    
    function saveToken($user_id, $token) {
        
        $this->db->where('id', $user_id);
        $this->db->set('token', $token);
        $this->db->update('tb_user');
    }    
    
    function getToken($user_id) {
        
        return $this->db->where('id', $user_id)->get('tb_user')->row()->token;
        
    }
    
    function setInvite($user_id, $group_id, $email) {
        
        $this->db->where('user_id', $user_id);
        $this->db->where('group_id', $group_id);
        $query = $this->db->get('tb_invite');
        if ($query->num_rows() == 0) {
            $this->db->set('user_id', $user_id);
            $this->db->set('group_id', $group_id);
            $this->db->set('status', 0);
            $this->db->set('is_paid', 0);
            $this->db->set('email', $email);
            $this->db->insert('tb_invite');
            $this->db->insert_id();
        }
    }
    
    function registerInvite($email, $group_id) {
        
        $this->db->set('user_id', -1);
        $this->db->set('group_id', $group_id);
        $this->db->set('status', 0);
        $this->db->set('is_paid', 0);
        $this->db->set('email', $email);
        $this->db->insert('tb_invite'); 
        $this->db->insert_id();
    }
    
    function checkInvite($user_id, $email) {
        
        $query = $this->db->where('email', $email)->get('tb_invite');
        
        if ($query->num_rows() > 0) {
            $this->db->where('email', $email);
            $this->db->set('user_id', $user_id);
            $this->db->update('tb_invite');
        }
    }
    
    /*
    function getAllPublicGroupInfo($user_id) {        
        
        $result = array();
        $this->db->select('tb_group.*');
        $this->db->from('tb_group');
        $this->db->where('tb_group.kind_group', 'public');
        $this->db->join('tb_invite', 'tb_group.id = tb_invite.group_id');        
        $this->db->where('tb_invite.user_id !=',$user_id);
        $this->db->where('tb_invite.status', 0);
        $this->db->group_by('tb_group.id');
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            
            foreach($query->result() as $group) {
                
                $arr = array('id' => $group->id, 
                             'creatorName' => $group->creator_name,
                             'creatorId' => $group->user_id,
                             'groupName' => $group->group_name,
                             'kindOfGroup' => $group->kind_group,
                             'groupType' => $group->group_type,
                             'pointParameter' => $group->point_parameter,
                             'price' => $group->price,
                             'startDate' => $group->start_date,
                             'endDate' => $group->end_date);
                array_push($result, $arr);
            }
        }
        
        return $result;
    }
    */
    
    function getAllPublicGroupInfo($user_id) {        
        
        $result = array();
        $this->db->where('kind_group', 'public');
        $query = $this->db->get('tb_group');
        
        if ($query->num_rows() > 0) {
            
            foreach($query->result() as $group) {
                
                $this->db->where('user_id', $user_id);
                $this->db->where('status', 1);
                $this->db->where('group_id', $group->id);
                $invie_query = $this->db->get('tb_invite');
                if ($invie_query->num_rows() == 0) {                
                    $arr = array('id' => $group->id, 
                                 'creatorName' => $group->creator_name,
                                 'creatorId' => $group->user_id,
                                 'groupName' => $group->group_name,
                                 'kindOfGroup' => $group->kind_group,
                                 'groupType' => $group->group_type,
                                 'pointParameter' => $group->point_parameter,
                                 'price' => $group->price,
                                 'startDate' => $group->start_date,
                                 'endDate' => $group->end_date);
                    array_push($result, $arr);
                }
            }
        }
        
        return $result;
    }
    
    function getInvitedGroup($user_id) {
        
        $result = array();
        $this->db->select('*');
        $this->db->from('tb_invite');          
        $this->db->join('tb_group', 'tb_invite.group_id = tb_group.id');
        $this->db->where('tb_invite.user_id', $user_id);
        $this->db->where('tb_invite.status', 0);
        
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            
            foreach($query->result() as $group) {
    
                $arr = array('id' => $group->id, 
                             'creatorName' => $group->creator_name,
                             'creatorId' => $group->user_id,
                             'groupName' => $group->group_name,
                             'kindOfGroup' => $group->kind_group,
                             'groupType' => $group->group_type,
                             'pointParameter' => $group->point_parameter,
                             'price' => $group->price,
                             'startDate' => $group->start_date,
                             'endDate' => $group->end_date);
                array_push($result, $arr);
            }             
        }
        
        return $result;
        
    }
    
    function getInvitedUserList($group_id) {
        
        $result = array();
        
        $this->db->select('*');
        $this->db->from('tb_invite');
        $this->db->where('tb_invite.group_id', $group_id);
        //$this->db->where('tb_invite.status', 0);
        $this->db->join('tb_user', 'tb_invite.user_id = tb_user.id');
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            
            foreach($query->result() as $user) {
    
                $arr = array('id' => $user->id, 
                             'userName' => $user->username,
                             'email' => $user->email,
                             'status' => $user->status == 0 ? 'Invited' : 'Joined',
                             );
                array_push($result, $arr);
            }             
        }
        
        return $result;       
        
    }
    
    function joinGroup($user_id, $group_id) {
        
        $this->db->where('user_id', $user_id);
        $this->db->where('group_id', $group_id);
        $query = $this->db->get('tb_invite');
        if ($query->num_rows() > 0) {
            
            $this->db->where('user_id', $user_id);
            $this->db->where('group_id', $group_id);
            $this->db->set('status', 1);
            $this->db->update('tb_invite');
        } else {
            $this->db->set('user_id', $user_id);
            $this->db->set('group_id', $group_id);
            $this->db->set('status', 1);
            $this->db->insert('tb_invite');
            $this->db->insert_id();
        }        
    }
    
    function getGroupUserTokens($group_id) {
        
        $user_tokens = array(); 
        
        $g_query = $this->db->where('id', $group_id)->get('tb_group');
        
        // if group is public
        if ($g_query->row()->kind_group == 'public') {
            $user_query = $this->db->get('tb_user');
            if ($user_query->num_rows() > 0) {
                foreach ($user_query->result() as $user) {
                    if (strlen($user->token) > 0)
                        array_push($user_tokens, $user->token);
                }     
            }    
        
        // if group is private
        } else {
        
            $this->db->select('tb_user.token');
            $this->db->from('tb_invite');
            $this->db->where('tb_invite.group_id', $group_id);
            $this->db->where('tb_invite.status', 1);
            $this->db->join('tb_user', 'tb_user.id = tb_invite.user_id');
            $query = $this->db->get();
            
            if ($query->num_rows() > 0) {
                
                foreach ($query->result() as $user) {
                    if (strlen($user->token) > 0)   
                        array_push($user_tokens, $user->token);
                }
            } 
        }
        
        return $user_tokens;
    }
    
    function sendMessage($user_id, $group_id, $message) {
        
        $result = array();
        
        $this->db->set('user_id', $user_id);
        $this->db->set('group_id', $group_id);
        $this->db->set('message', $message);
        $this->db->set('reg_date', 'NOW()', false);
        $this->db->insert('tb_message');
        $this->db->insert_id();        
        
        $query = $this->db->where('id', $user_id)->get('tb_user');
        
        if ($query->num_rows() > 0) {
            
            $row = $query->row();
            $result['userId'] = $row->id;
            $result['photoUrl'] = $row->photo_url;
            $result['userName'] = $row->username;
            $result['messageContent'] = $message;
        }
        
        return $result;
        
    }
    
    function getMessages($group_id, $read_count) {
        
        $result = array();
                
        $this->db->select('tb_message.*, tb_user.username, tb_user.photo_url');
        $this->db->from('tb_message');
        $this->db->where('tb_message.group_id', $group_id);
        $this->db->join('tb_user', 'tb_message.user_id = tb_user.id');
        
        $query = $this->db->get();
        
        if ( $query->num_rows() > 0 ) {
            
            foreach($query->result() as $message) {
                
                $arr = array('messageId' => $message->id,
                             'userId' => $message->user_id,
                             'userName' => $message->username,
                             'photoUrl' => $message->photo_url,
                             'messageContent' => $message->message);
                array_push($result, $arr);
            }
        }
        return $result;
        //return array_reverse($result);
        
    }
    
    function getAllGroupList($user_id) {
    
        $result = array();
        
        $this->db->select('*');
        $this->db->from('tb_invite');
        $this->db->where('tb_invite.user_id', $user_id);
        $this->db->where('tb_invite.status', 1); 
        $this->db->join('tb_group', 'tb_invite.group_id = tb_group.id');
        $query = $this->db->get();        
        //$query1 = $this->db->where('user_id', $user_id)->get('tb_group');          
        
        //if ($query->num_rows() > 0 || $query1->num_rows() > 0 ) {
        if ($query->num_rows() > 0 ) {    
            //$q_result = array_merge($query->result(), $query1->result());
            foreach($query->result() as $group) {
            //foreach($q_result as $group) {
                
                if ($this->checkFinished($group->end_date) != 1) {
                    
                    $arr = array('id' => $group->id, 
                             'creatorName' => $group->creator_name,
                             'creatorId' => $group->user_id,
                             'groupName' => $group->group_name,
                             'kindOfGroup' => $group->kind_group,
                             'groupType' => $group->group_type,
                             'pointParameter' => $group->point_parameter,
                             'price' => $group->price,
                             'startDate' => $group->start_date,
                             'endDate' => $group->end_date,
                             'challengeFinished' => $this->checkFinished($group->end_date));
                    array_push($result, $arr);                     
                }
                

            }
        }       
        
        return $result;
    }
    
    function checkFinished($endDate) {
        
        $today = new DateTime(); // This object represents current date/time
        $today->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison

        $match_date = DateTime::createFromFormat( "Y/m/d", $endDate );
        $match_date->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison

        $diff = $today->diff( $match_date );
        $diffDays = (integer)$diff->format( "%R%a" ); // Extract days count in interval
        

        /*
        switch( $diffDays ) {
            case 0:
                echo "//Today";
                break;
            case -1:
                echo "//Yesterday";
                break;
            case +1:
                echo "//Tomorrow";
                break;
            default:
                echo "//Sometime";
        }*/
        if ($diffDays < -2) {
            return 1;
        } else {
            return 0;
        }
    }
    
    function getUserInfoById($user_id) {
        
        $user = array();
        $query = $this->db->where('id', $user_id)->get('tb_user');
        
        if ($query->num_rows() > 0) {
            
            $row = $query->row();
            $user['userId'] = $row->id;
            $user['photoUrl'] = $row->photo_url;
            $user['userName'] = $row->username;
        }
        
        return $user;
    }
    
    function updateMacroPoint($user_id, $body_fat, $body_mass, $daily_protein, $daily_carb, $daily_fat, $total_blocks, $starting_bdoy_fat_per) {
        
            $this->db->where('user_id', $user_id);
            $this->db->set('body_fat' ,$body_fat);
            $this->db->set('body_mass' ,$body_mass);
            $this->db->set('daily_protein' ,$daily_protein);
            $this->db->set('daily_carb' ,$daily_carb);
            $this->db->set('daily_fat' ,$daily_fat);
            $this->db->set('total_blocks' ,$total_blocks);
            $this->db->set('starting_bdoy_fat_percent' ,$starting_bdoy_fat_per);
            $this->db->update('tb_macro');
    }
    
    function saveEatBlocksTillNow($user_id, $eaten_blocks) {
        
        $today = date('Y-m-d');
        $this->db->where('user_id', $user_id);
        $this->db->set('eaten_blocks', $eaten_blocks);
        $this->db->set('block_uptodate', $today);
        $this->db->update('tb_macro');
    }
    
    function getEatBlocksTillNow($user_id) {
        
        $today = date('Y-m-d');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('tb_macro');
        
        if ($query->num_rows() > 0) {
            
            if(strtotime($query->row()->block_uptodate) == strtotime($today)) {
                return $query->row()->eaten_blocks;    
            } else {
                $this->resetEatBlocks($user_id);
                return 0;
            }
            
        } else {
            return 0;
        }
        
    }
    
    function resetEatBlocks($user_id) {
        
        $today = date('Y-m-d');
        $this->db->where('user_id', $user_id);
        $this->db->set('eaten_blocks', 0);
        //$this->db->set('total_blocks', 0);
        $this->db->set('block_uptodate', $today);
        $this->db->update('tb_macro');
    }
    
    function addPoint($user_id, $group_id, $point_count) {
        
        $this->db->where('user_id', $user_id);
        $this->db->where('group_id', $group_id);
        $query = $this->db->get('tb_point');
        if ($query->num_rows() > 0) {
//            $yesterday_point = $query->row()->today_point;
//            $total_point = $query->row()->total_point;
            $this->db->where('user_id', $user_id);
            $this->db->where('group_id', $group_id);             
            $this->db->set('today_point', 'today_point+'.$point_count, FALSE);
            $this->db->set('total_point', 'total_point+'.$point_count, FALSE);
            $this->db->set('reg_date', 'NOW()', false);
            $this->db->update('tb_point');
        } else {
            $this->db->set('user_id', $user_id);
            $this->db->set('group_id', $group_id);
            $this->db->set('today_point', $point_count);
            $this->db->set('total_point', $point_count);
            $this->db->set('reg_date', 'NOW()', false);
            $this->db->insert('tb_point');
            $this->db->insert_id();
        }
    }
    
    function deletePoint($user_id, $group_id, $pointCnt) {
        
        $this->db->where('user_id', $user_id);
        $this->db->where('group_id', $group_id);
        $query = $this->db->get('tb_point');
        $today_point = 0;
        if ($query->num_rows() > 0) {
            $yesterday_point = $query->row()->today_point;
            if ($yesterday_point < 1) $yesterday_point = 1;
            if ($yesterday_point-$pointCnt < 0) {
                $today_point = 0;
            } else {
                $today_point = $yesterday_point-$pointCnt;
            }            
            
            $total_point = $query->row()->total_point;            
            
            if ($total_point-$pointCnt < 0) {
                $today_point = 0;
            } else {
                $total_point = $total_point-$pointCnt;
            }
            
            $this->db->where('user_id', $user_id);
            $this->db->where('group_id', $group_id);            
            $this->db->set('today_point', $today_point);
            $this->db->set('total_point', $total_point);
            $this->db->update('tb_point');
        } 
    }
    
    function getPointModel($user_id) {
        
        $result = array();
        
        $today = date('Y-m-d');
        
        $this->db->where('user_id', $user_id);
        $this->db->where('reg_date <', $today);
        $this->db->set('today_point', 0);
        $this->db->update('tb_point');
        
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('tb_point');
        
        if ($query->num_rows() > 0) {
            
            foreach($query->result() as $point) {
                
                $arr = array('groupId' => $point->group_id,
                             'todayPoints' => $point->today_point,
                             'totalPoints' => $point->total_point);
                array_push($result, $arr);
            }
        } 
        return $result;
    }
    
    function uploadPhoto($user_id, $file_url) {
        
        $this->db->where('id', $user_id);
        $this->db->set('photo_url', $file_url);
        $this->db->update('tb_user');
    }
    
    function getRankingListInGroup($group_id) {
        
        $result = array();
        $this->db->select('tb_user.id, tb_user.username, tb_user.photo_url, tb_point.total_point, tb_point.benchmark');
        $this->db->from('tb_point');        
        $this->db->join('tb_user', 'tb_user.id = tb_point.user_id');
        $this->db->where('tb_point.group_id', $group_id);
        $this->db->order_by('tb_point.total_point', 'DESC');
        $query = $this->db->get();
        
        if ($query->num_rows() > 0 ) {
            
            foreach($query->result() as $row) {
                $arr = array('userId' => $row->id,
                             'totalPoints' => $row->total_point,
                             'userName' => $row->username,
                             'photoUrl' => $row->photo_url,
                             'benchmark' => $row->benchmark);
                array_push($result, $arr); 
            }
        }
        
        return $result;
    }
                             
    function giveBenchmark($group_id, $user_id) { 

        $this->db->where('user_id', $user_id);
        $this->db->where('group_id', $group_id);     
        //$this->db->set('total_point', 'total_point+5', FALSE);
        $this->db->set('benchmark', 1);
        $this->db->set('reg_date', 'NOW()', false);
        $this->db->update('tb_point'); 
    }
    
    function setPaidStatus($user_id, $group_id) {
        
        $this->db->where('user_id', $user_id);
        $this->db->where('group_id', $group_id);
        $this->db->set('is_paid', 1);
        $this->db->update('tb_invite');
        
    }
    
    function getLibrary() {
        
        return $this->db->get('tb_library')->result_array();
    }
    
    function getHelp() {
        
        return $this->db->get('tb_help')->result_array();
    }
    
    function updateMessage($message_id, $message_content) {
        
        $this->db->where('id', $message_id);
        $this->db->set('message', $message_content);
        $this->db->set('blocked_at', 'NOW()', false);
        $this->db->update('tb_message');
    }
    
    function getPreviousChallenges($user_id) {
    
        $result = array();
        
        $this->db->select('*');
        $this->db->from('tb_invite');
        $this->db->where('tb_invite.user_id', $user_id);
        $this->db->where('tb_invite.status', 1); 
        $this->db->join('tb_group', 'tb_invite.group_id = tb_group.id');
        $query = $this->db->get();        
        //$query1 = $this->db->where('user_id', $user_id)->get('tb_group');          
        
        //if ($query->num_rows() > 0 || $query1->num_rows() > 0 ) {
        if ($query->num_rows() > 0 ) {    
            //$q_result = array_merge($query->result(), $query1->result());
            foreach($query->result() as $group) {
            //foreach($q_result as $group) {
                
                if ($this->checkFinished($group->end_date) == 1) {
                    
                    $arr = array('id' => $group->id, 
                                'creatorName' => $group->creator_name,
                                'creatorId' => $group->user_id,
                                'groupName' => $group->group_name,
                                'kindOfGroup' => $group->kind_group,
                                'groupType' => $group->group_type,
                                'pointParameter' => $group->point_parameter,
                                'price' => $group->price,
                                'startDate' => $group->start_date,
                                'endDate' => $group->end_date,
                                'challengeFinished' => $this->checkFinished($group->end_date));
                    array_push($result, $arr);                    
                    
                }                
            }
        }       
        
        return $result;
    }
}
?>
