<?php
require_once('Mail.php');
  require_once 'vendor/autoload.php';

  use net\authorize\api\contract\v1 as AnetAPI;
  use net\authorize\api\controller as AnetController;

  define("AUTHORIZENET_LOG_FILE", "phplog");

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller  {


    function __construct(){

        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('api_model');
        $this->load->library('session');     
    }
    
    /**
     * Make json response to the client with result code message
     *
     * @param p_result_code : Result code
     * @param p_result_msg : Result message
     * @param p_result : Result json object
     */

    private function doRespond($p_result_code,  $p_result){

         $p_result['resultCode'] = $p_result_code;

         $this->output->set_content_type('application/json')->set_output(json_encode($p_result));
    }

    /**
     * Make json response to the client with success.
     * (result_code = 0, result_msg = "success")
     *
     * @param p_result : Result json object
     */

    private function doRespondSuccess($result){

         $this->doRespond(100, $result);
    }

    /**
     * Url decode.
     *
     * @param p_text : Data to decode
     *
     * @return text : Decoded text
     */

    private function doUrlDecode($p_text){

        $p_text = urldecode($p_text);
        $p_text = str_replace('&#40;', '(', $p_text);
        $p_text = str_replace('&#41;', ')', $p_text);
        $p_text = str_replace('%40', '@', $p_text);
        $p_text = str_replace('%20',' ', $p_text);
        $p_text = trim($p_text);


        return $p_text;
    }
     
    function sendPush($user_id, $type, $body, $content) {
        
        // send FCM push notification
        
        $url = "https://fcm.googleapis.com/fcm/send";
        $api_key = "AAAA_q6-rEA:APA91bFMVI95McE26k71yJV3fzgriMXjVa-fgUdfyozR_7G6-a3b7oWTpzpualrJINg1VWyXb32Rxe0PXg1vKG0XK7T3AhhXf0YvFb07ivJciC4psDq4JWOmW53WX-cJr0f4zsYx0sTc";
        
        $token = "";
        $token = $this->api_model->getToken($user_id);
        
        if (strlen($token) == 0 ) {

            return;
        }
        
//       if(is_array($target)){
//        $fields['registration_ids'] = $target;
//       }else{
//        $fields['to'] = $target;
//           }

        // Tpoic parameter usage
//        $fields = array
//                    (
//                        'to'  => '/topics/alerts',
//                        'notification'          => $msg
//                    );
        $data = array('msgType' => $type,
                      'content' => $content);
        
        $msg = array
                (
                    'body'     => $body,
                    'title'    => 'Transform',   
                    'badge' => 1,             
                    'sound' => 'default'/*Default sound*/
                );
        $fields = array
            (
                //'registration_ids'    => $tokens,
                'to'                => $token,
                'notification'      => $msg,
                'priority'          => 'high',
                'data'              => $data
            );

        $headers = array(
            'Authorization: key=' . $api_key,
            'Content-Type: application/json'
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);

        //@curl_exec($ch);
        
        $result['result'] = curl_exec($ch); 

        curl_close($ch); 
        
        return $result;
        
    } 
    
    function sendPushAll($tokens, $group_id, $type, $body, $content) {
        
        // send FCM push notification
        
        $url = "https://fcm.googleapis.com/fcm/send";
        $api_key = "AAAA_q6-rEA:APA91bFMVI95McE26k71yJV3fzgriMXjVa-fgUdfyozR_7G6-a3b7oWTpzpualrJINg1VWyXb32Rxe0PXg1vKG0XK7T3AhhXf0YvFb07ivJciC4psDq4JWOmW53WX-cJr0f4zsYx0sTc";
        
        //        $tokens = array();
        //        foreach ($user_ids as $user_id) {
        //            array_push($tokens, $this->api_model->getToken($user_id));
        //        }
        if (count($tokens) == 0 ) {

            return;
        }

        $data = array('msgType' => $type,
                      'groupId' => $group_id,  
                      'content' => json_encode($content));
        
        $msg = array
                (
                    'body'     => $body,
                    'title'    => 'Transform',   
                    'badge' => 1,             
                    'sound' => 'default'/*Default sound*/
                );
        $fields = array
            (
                'registration_ids'  => $tokens,
                //'to'                => $token,
                'notification'      => $msg,
                'priority'          => 'high',
                'data'              => $data
            );

        $headers = array(
            'Authorization: key=' . $api_key,
            'Content-Type: application/json'
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);

        //@curl_exec($ch);
        
        $p_result  = curl_exec($ch);
        
        if ($p_result === false) {
            $result['error'] = curl_error($ch);
        }
        $result['result'] = $p_result;        
        
        curl_close($ch); 
        
        return $result;
        
    } 
     
    function signup() {
         
         $result = array();         
         $username = $_POST['userName'];
         $email = $_POST['email'];
         $password = $_POST['password'];
         
         $email_exist = $this->api_model->email_exist($email);
         
         if ($email_exist > 0) {
             $this->doRespond(103, $result);
             return;
         }
         
         if(!function_exists('password_hash'))
             $this->load->helper('password');

         $password_hash = password_hash($password, PASSWORD_BCRYPT);
         
         $user_id = $this->api_model->register($username, $email, $password_hash);
         $result['userId'] = $user_id;
         
         // check it this user received invitation
         $this->api_model->checkInvite($user_id, $email);
         
         $this->doRespondSuccess($result);
    }
     
    function login() {
         
         $result = array();  
         $email = $_POST['email'];
         $password = $_POST['password'];
         
         $email_exist = $this->api_model->email_exist($email);
         
         if ($email_exist == 0) {             
             $this->doRespond(101, $result);
             return;
         }
         
         if(!function_exists('password_verify'))
             $this->load->helper('password'); // password_helper.php loading

         $row = $this->db->get_where('tb_user', array('email'=>$email))->row();
         $pwd = $row->password;

         if (!password_verify($password, $pwd)){  // wrong password.
             
             $this->doRespond(102, $result);
             return;

         } else {
             
             $result['userModel'] = array('id' => $row->id,
                                          'userName' => $row->username,
                                          'email' => $email,
                                          'password' => $password,
                                          'photoUrl' =>$row->photo_url
                                          ); 
             $result['propertyModel'] = $this->api_model->getPropoerty($row->id);                        
             $result['macroModel'] = $this->api_model->getMacroPoint($row->id);
             $result['pointModelList'] = $this->api_model->getPointModel($row->id);                        
         }

         $this->doRespondSuccess($result);  
         
    }
     
    function uploadPersonalInfo(){
         
         $result = array();                       
         
         $user_id = $_POST['id'];
         $gender = $_POST['gender'];
         $birthday = $_POST['birthday'];
         $age = $_POST['age'];
         $body_weight = $_POST['bodyWeight'];
         $height = $_POST['height'];
         $activity_factor = $_POST['activityFactor'];
         $wrist_measurement = $_POST['wristMeasurement'];
         $waist_measurement = $_POST['waistMeasurement'];
         $average_hip = $_POST['averageHip'];
         $average_abdomen = $_POST['averageAbdomen'];
         $starting_bodyweight = $_POST['startingBodyWeight'];
         
         $this->api_model->uploadPersonalInfo($user_id, $gender, $birthday, $age, $body_weight, $height, $activity_factor,
                                                $wrist_measurement, $waist_measurement, $average_hip, $average_abdomen, $starting_bodyweight);
         
         $this->doRespondSuccess($result);
    }
     
    function loginWithSocial() {
         
         $result = array();         
         $username = $_POST['userName'];
         $email = $_POST['email'];
         $password = $_POST['password'];
         
         $email_exist = $this->api_model->email_exist($email);

         
         if ($email_exist > 0) {
             
             $row = $this->db->get_where('tb_user', array('email'=>$email))->row();
             $result['userModel'] = array('id' => $row->id,
                                          'userName' => $row->username,
                                          'email' => $email,
                                          'password' => $password,
                                          'photoUrl' =>$row->photo_url
                                          );
             $result['propertyModel'] = $this->api_model->getPropoerty($row->id); 
             $result['macroModel'] = $this->api_model->getMacroPoint($row->id);
             $result['pointModelList'] = $this->api_model->getPointModel($row->id);
             $this->doRespondSuccess($result);
             return;  
         } else {
             if(!function_exists('password_hash'))
             $this->load->helper('password');

             $password_hash = password_hash($password, PASSWORD_BCRYPT);
             $user_id = $this->api_model->registerWithSocial($username, $email, $password_hash);
             $result['userId'] = $user_id;
             // check it this user received invitation
             $this->api_model->checkInvite($user_id, $email);   
         }
         
         $this->doRespond(104, $result);
     }
     
    function createGroup() {
         
         $result = array();
         $user_id = $_POST['userId'];
         $creator_name = $_POST['creatorName'];
         $group_name = $_POST['groupName'];
         $kind_group = $_POST['kindOfGroup'];
         $group_type = $_POST['groupType'];
         $point_param = $_POST['pointParameter'];
         $price = $_POST['price'];
         $start_date = $_POST['startDate'];
         $end_date = $_POST['endDate'];
         
         $exist_group_id =  $this->api_model->groupExist($group_name);
         if ($exist_group_id > 0) {
             $result['groupId'] =  $exist_group_id;
             $this->doRespond(105, $result);
             return;
         }
         
         $result['groupId'] = $this->api_model->createGroup($user_id, $creator_name, $group_name, $kind_group, $group_type, $point_param, $price, $start_date, $end_date);
         $this->doRespondSuccess($result);          
    }
     
    function getUserInfoByName() {
         
         $result = array();
         $username = $_POST['userName'];
         
         $result['userModelList'] = $this->api_model->getUserInfoByName($username);
         
         $this->doRespondSuccess($result);
    }
     
    function saveToken() {
         
         $result = array();
         
         $user_id = $_POST['userId'];
         $token = $_POST['token'];
         
         $this->api_model->saveToken($user_id, $token);
         
         $this->doRespondSuccess($result); 
    }
    
    /// for iOS, If user with email is exist, then call sendPushForInvite API,
    /// otherwise send mail and return result code for non exist user.
    function addMember() {
        
        $result = array();
        $user_id = $_POST['userId'];
        $creator_name= $_POST['creatorName'];
        $email = $_POST['email'];
        $group_id = $_POST['groupId'];
        
        if ($this->api_model->email_exist($email) > 0) {
           
            $this->api_model->setInvite($user_id, $group_id, $email);
            $msg = 'You received invitation';
            $type = 'invite';
            
            // send mail here
            $this->sendInviteMail($email, $creator_name);
            $result = $this->sendPush($user_id, $type, $msg, $msg);                                                                                                 
            
            $this->doRespondSuccess($result);
        } else {
            // If user does not exist, keep in invite table
            
            $this->api_model->registerInvite($email, $group_id);
            
            // send mail here
            $this->sendInviteMail($email, $creator_name);
            $this->doRespond(101, $result);                                                                                                                                                     
        }
    }
    
    // send mail function
    function sendInviteMail($email, $creator_name) {
        
        $result = array();
        

        $to =  $this->doUrlDecode($email);
        
        $host = "smtp.sparkpostmail.com";  // it will be  18.216.154.87 or localhost
        $port = "587";  // same secure TLS port
        $username = "SMTP_Injection"; // ssending email address. example: info@transformnutritionchallenges.com
        $password = "fdb5665c2741a4d709ec40e77ff920d1b759dc04"; // i'll set it after mail server setup

        $subject = "Transform Invitation";
        $message = "Hello!<br><br>
        You have been invited to join a nutrition challenge group by $creator_name, using the Transform app!<br>  
        You will be able to see this invitation and group details in the “Join a Group” page of the App.<br>
        If you do not already have the app, please download and enjoy.<br>Good luck!<br><br>
        
        iOS : https://itunes.apple.com/us/app/transform-challenges/id1310828601?ls=1&mt=8 <br><br>
        Android : https://play.google.com/store/apps/details?id=com.sts.transform   <br><br>

        If you would like some more information on Transform, please visit our website at http://www.transformnutritionchallenges.com<br><br><br>

        -Crew at Transform";

        $from = "Transform Nutrition <info@transformnutritionchallenges.com>";
        $headers = array ('From' => $from,
                          'To' => $to,
                          'Subject' => $subject,
                          'MIME-Version' => '1.0',
                          'Content-type' => "text/html;charset=UTF-8");

        $smtp = Mail::factory('smtp',
          array ('host' => $host,
            'port' => $port,
            'auth' => true,
            'username' => $username,
            'password' => $password));
        
        if($smtp->send($to, $headers, $message)) {             
               
                $this->doRespondSuccess($result);
                        
        }   else {             
            $this->doRespond(105, $result);
        }
        
    }
    
    function sendPushForInvite()  {
        
        $result = array();
        
        $user_id = $_POST['userId'];
        $group_id = $_POST['groupId'];
        $creator_name= $_POST['creatorName'];
        $email = $_POST['email'];
        
        if ($this->api_model->email_exist($email) > 0) {
           
            $this->api_model->setInvite($user_id, $group_id, $email);
            $msg = 'You received invitation';
            $type = 'invite';
            
            $this->sendInviteMail($email, $creator_name);
            $result = $this->sendPush($user_id, $type, $msg, $msg);                                                                                                 
            
            $this->doRespondSuccess($result);
        } else {
            
            // If user does not exist, keep in invite table                 
            $this->api_model->registerInvite($email, $group_id);
            
            // send mail here
            $this->sendInviteMail($email, $creator_name);
            $this->doRespond(101, $result);                                                                                                                                                     
        }       
                
    }
    
    /// for iOS, get ALL group that can be available in join group page
    
    function getJoinGroupInfo() {
        
        $result = array();
        
        $user_id = $_POST['userId'];
        
        $result['publicGroup'] =  $this->api_model->getAllPublicGroupInfo($user_id);
        $result['invitedGroup'] = $this->api_model->getInvitedGroup($user_id);
        
        $this->doRespondSuccess($result);
    }
    
    function getAllPublicGroupInfo() {
        
        $result = array();
        $user_id = $_POST['userId'];         
        
        $result['groupList'] = $this->api_model->getAllPublicGroupInfo($user_id);
        
        $this->doRespondSuccess($result);
    }
    
    function getInvitedGroupInfo() {
        
        $result = array();
        $user_id = $_POST['userId'];
        
        $result['groupList'] = $this->api_model->getInvitedGroup($user_id);
        
        $this->doRespondSuccess($result);
    }
    
    function getUserListInGroup() {
        
        $result = array();
        
        $group_id = $_POST['groupId'];
        
        $result['userList'] = $this->api_model->getInvitedUserList($group_id);
        
        $this->doRespondSuccess($result);
        
    }
    
    function joinGroup() {
        
        $result = array();
        $user_id = $_POST['userId'];
        $group_id = $_POST['groupId'];
        
        $this->api_model->joinGroup($user_id, $group_id);
        
        $this->doRespondSuccess($result);
        
    }
    
    function sendMessage() {
        
        $result = array();
        $user_tokens = array();
        $content = array();
        
        $user_id = $_POST['userId'];
        $group_id = $_POST['groupId'];
        $message = $_POST['messageContent'];
        
        /// record message and return message model
        $content = $this->api_model->sendMessage($user_id, $group_id, $message);
        
        //$message = "You receoved message";
        $user_tokens = $this->api_model->getGroupUserTokens($group_id);
        
        $result['push'] = $this->sendPushAll($user_tokens, $group_id, "chat", $message, $content);
        
        $result['content'] = $content;
        $result['tokens'] = $user_tokens;
        $this->doRespondSuccess($result);
    }
    
    function getMessages() {
        
        $result = array();
        
        $group_id = $_POST['groupId'];
        $read_count = $_POST['readCount'];
        
        $result['chatList'] = $this->api_model->getMessages($group_id, $read_count);
        
        $this->doRespondSuccess($result);         
    }
    
    function getAllGroupList() {
        
        $result = array();
        $user_id = $_POST['userId'];
        
        $result['groupList'] = $this->api_model->getAllGroupList($user_id);
        
        $this->doRespondSuccess($result);
    }
    
    function updateMacroPoint() {
        
        $result = array();
        
        $user_id = $_POST['userId'];
        $body_fat = $_POST['bodyFat'];
        $body_mass = $_POST['bodyMass'];
        $daily_protein = $_POST['dailyProtein'];
        $daily_carb = $_POST['dailyCarb'];
        $daily_fat = $_POST['dailyFat'];
        $total_blocks = $_POST['totalBlocks'];
        $starting_bdoy_fat_per = $_POST['startingBodyFatPercent'];
        
        $this->api_model->updateMacroPoint($user_id, $body_fat, $body_mass, $daily_protein, $daily_carb, $daily_fat, $total_blocks, $starting_bdoy_fat_per);
        
        $this->doRespondSuccess($result);
    } 
    
    function saveEatBlocksTillNow() {
        
        $result = array();
        
        $user_id = $_POST['userId'];
        $eaten_blocks = $_POST['eatBlocksTillNow'];         
        
        $this->api_model->saveEatBlocksTillNow($user_id, $eaten_blocks);
        
        $this->doRespondSuccess($result);        
    }
    
    function getEatBlocksTillNow() {
    
        $result = array();
        
        $user_id = $_POST['userId'];
        
        $eatBlocksTillNow =  $this->api_model->getEatBlocksTillNow($user_id);
        //if ($eatBlocksTillNow > 0 ) {
            
            $result['eatBlocksTillNow'] =  $eatBlocksTillNow;        
            $this->doRespondSuccess($result);
        //} else {
        //    $this->doRespond(105, $result);
        //}                 
    }
    
    function addPoint() {
        
        $result = array();
        
        $user_id = $_POST['userId'];
        $group_id = $_POST['groupId'];
        $point_count = $_POST['pointCnt'];
        
        $this->api_model->addPoint($user_id, $group_id, $point_count);
        
        $this->doRespondSuccess($result);        
    }
    
    function deletePoint() {
        
        $result = array();
        
        $user_id = $_POST['userId'];
        $group_id = $_POST['groupId'];
        $point_count = $_POST['pointCnt'];
        
        $this->api_model->deletePoint($user_id, $group_id, $point_count);
        
        $this->doRespondSuccess($result);        
    }
               
    function uploadPhoto() {
         
         $result = array();
         
         $user_id = $_POST['userId'];
                  
         if(!is_dir("uploadfiles/")) {
             mkdir("uploadfiles/");
         }
         $upload_path = "uploadfiles/";  

         $cur_time = time();
         
         $dateY = date("Y", $cur_time);
         $dateM = date("m", $cur_time);
         
         if(!is_dir($upload_path."/".$dateY)){
             mkdir($upload_path."/".$dateY);
         }
         if(!is_dir($upload_path."/".$dateY."/".$dateM)){
             mkdir($upload_path."/".$dateY."/".$dateM);
         }
         
         $upload_path .= $dateY."/".$dateM."/";
         $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 4000,
            'max_height' => 3000,
            'file_name' => $user_id.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('file')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $id = $this->api_model->uploadPhoto($user_id, $file_url);
            $result['photoUrl'] = $file_url;
            $this->doRespondSuccess($result);

        } else {

            $this->doRespond(206, $result);// upload fail
            return;
        }     
    }
    
    function getRankingListInGroup() {
        
        $result = array();
        $group_id = $_POST['groupId'];
        
        $result['rankingList'] = $this->api_model->getRankingListInGroup($group_id);
        
        $this->doRespondSuccess($result);
    }
    
    function giveBenchmark() {
        
        $result = array();
        $group_id = $_POST['groupId'];
        $user_id = $_POST['userId'];
        
        $this->api_model->giveBenchmark($group_id, $user_id);
        $this->api_model->addPoint($user_id, $group_id, 5);
        //-------
        $msg = 'you received benchmark point';
        $type = 'benchmark';
        $result = $this->sendPush($user_id, $type, $msg, $msg);
        $this->doRespondSuccess($result);
    }
    /// for test
    function getPointTest($userId) {
        
        $result = array();
        $result = $this->api_model->getPointModel($userId);
        $this->doRespondSuccess($result);
    }
    
    function paymentProcess()  {
        
        $result = array();
        
        $user_id = $_POST['userId'];
        $group_id = $_POST['groupId'];
        $user_name = $_POST['userName'];
        $email = $_POST['email'];
        $card_number = $_POST['cardNumber'];
        $expiration_date = $_POST['expirationDate'];
        $card_code = $_POST['cardCode'];
        $amount = $_POST['amount'];
        
        define("AUTHORIZENET_SANDBOX", false);
        
        /* Create a merchantAuthenticationType object with authentication details
           retrieved from the constants file */
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        
        // Sandbox Mode
        //$merchantAuthentication->setName(SANDBOX_LOGIN_ID);
        //$merchantAuthentication->setTransactionKey(SANDBOX_TRANSACTION_KEY);
        
        
        /// Merchant mode
        $merchantAuthentication->setName(MERCHANT_LOGIN_ID);
        $merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);

        // Set the transaction's refId
        $refId = 'ref' . time();

        // Create the payment data for a credit card
        $creditCard = new AnetAPI\CreditCardType();
        $creditCard->setCardNumber($card_number);
        $creditCard->setExpirationDate($expiration_date);
        $creditCard->setCardCode($card_code);

        // Add the payment data to a paymentType object
        $paymentOne = new AnetAPI\PaymentType();
        $paymentOne->setCreditCard($creditCard);

        // Create order information
        $order = new AnetAPI\OrderType();
        $order->setInvoiceNumber($user_id.intval(microtime(true) * 10));
        $order->setDescription("Transform join group payment");

        // Set the customer's Bill To address
        $customerAddress = new AnetAPI\CustomerAddressType();
        $customerAddress->setFirstName($user_name);
        $customerAddress->setLastName("");
        $customerAddress->setCompany("");
        $customerAddress->setAddress("");
        $customerAddress->setCity("");
        $customerAddress->setState("");
        $customerAddress->setZip("");
        $customerAddress->setCountry("");

        // Set the customer's identifying information
        $customerData = new AnetAPI\CustomerDataType();
        $customerData->setType("individual");
        $customerData->setId($user_id);
        $customerData->setEmail($email);

        // Add values for transaction settings
        $duplicateWindowSetting = new AnetAPI\SettingType();
        $duplicateWindowSetting->setSettingName("duplicateWindow");
        $duplicateWindowSetting->setSettingValue("60");

        // Add some merchant defined fields. These fields won't be stored with the transaction,
        // but will be echoed back in the response.
        $merchantDefinedField1 = new AnetAPI\UserFieldType();
        $merchantDefinedField1->setName("");
        $merchantDefinedField1->setValue("");

        $merchantDefinedField2 = new AnetAPI\UserFieldType();
        $merchantDefinedField2->setName("");
        $merchantDefinedField2->setValue("");

        // Create a TransactionRequestType object and add the previous objects to it
        $transactionRequestType = new AnetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType("authCaptureTransaction");
        $transactionRequestType->setAmount($amount);
        $transactionRequestType->setOrder($order);
        $transactionRequestType->setPayment($paymentOne);
        $transactionRequestType->setBillTo($customerAddress);
        $transactionRequestType->setCustomer($customerData);
        $transactionRequestType->addToTransactionSettings($duplicateWindowSetting);
        $transactionRequestType->addToUserFields($merchantDefinedField1);
        $transactionRequestType->addToUserFields($merchantDefinedField2);

        // Assemble the complete transaction request
        $request = new AnetAPI\CreateTransactionRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setTransactionRequest($transactionRequestType);

        // Create the controller and get the response
        $controller = new AnetController\CreateTransactionController($request);     
        //$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
        $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);
   

        if ($response != null) {
            // Check to see if the API request was successfully received and acted upon
            
            if ($response->getMessages()->getResultCode() == RESPONSE_OK) {
                // Since the API request was successful, look for a transaction response
                // and parse it to display the results of authorizing the card
                $tresponse = $response->getTransactionResponse();

                if ($tresponse != null && $tresponse->getMessages() != null) {
                    
                    $result['status'] = "Successfully created transaction";
                    $result['TransactionID'] =   $tresponse->getTransId();
                    $result['ResponseCode'] =   $tresponse->getResponseCode();
                    $result['MessageCode'] =   $tresponse->getMessages()[0]->getCode();
                    $result['AuthCode'] =   $tresponse->getAuthCode();
                    $result['Description'] =   $tresponse->getMessages()[0]->getDescription();
                    $this->api_model->setPaidStatus($user_id, $group_id);
                    $this->doRespondSuccess($result);
                    /*
                    echo " Successfully created transaction with Transaction ID: " . $tresponse->getTransId() . "\n";
                    echo " Transaction Response Code: " . $tresponse->getResponseCode() . "\n";
                    echo " Message Code: " . $tresponse->getMessages()[0]->getCode() . "\n";
                    echo " Auth Code: " . $tresponse->getAuthCode() . "\n";
                    echo " Description: " . $tresponse->getMessages()[0]->getDescription() . "\n";
                    */
                } else {
                    
                    $result['status'] = "Transaction Failed";
                    $result['ErrorCode'] = $tresponse->getErrors()[0]->getErrorCode();
                    $result['ErrorMessage'] = $tresponse->getErrors()[0]->getErrorText();
                    
                    $this->doRespond(209, $result);
                    /*
                    echo "Transaction Failed \n";
                    if ($tresponse->getErrors() != null) {
                        echo " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
                        echo " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
                    }
                    */
                }
                // Or, print errors if the API request wasn't successful
            } else {
                //echo "Transaction Failed \n";
                $result['status'] = "Transaction Failed";
                $tresponse = $response->getTransactionResponse();
                $result['ErrorMessage'] = "Transaction Failed with error code". $response->getMessages()->getResultCode();

                //echo "messages\n" ;
                //print_r($tresponse);
                //print_r($response->getMessages() );
                
                if ($tresponse != null && $tresponse->getErrors() != null) {
                    
                    //$result['ErrorCode'] = $tresponse->getErrors()[0]->getErrorCode();
                    $result['ErrorMessage'] = "Transaction Failed with error code". $response->getMessages()->getResultCode();
                    //echo " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
                    //echo " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
                    $this->doRespond(209, $result);
                } else {
                    
                    //print_r ($tresponse);
                    //print_r ($tresponse->getResponse());
                    //$result['ErrorCode'] = $tresponse->getErrors()[0]->getErrorCode();
                    $result['ErrorMessage'] = "Transaction Failed with error code". $response->getMessages()->getResultCode();
                    $this->doRespond(209, $result);
                    //echo " Error Code  : " . $response->getMessages()->getMessage()[0]->getCode() . "\n";
                    //echo " Error Message : " . $response->getMessages()->getMessage()[0]->getText() . "\n";
                }
            }
        } else {
            //echo  "No response returned \n";
            $result['status'] = "No response returned";
            $result['ErrorMessage'] =  "No response returned";
            
            $this->doRespond(209, $result);
        }

        
        
        //return $response;
    }
    
    function getLibrary() {
        
        $result = array();
        
        $result['library'] = $this->api_model->getLibrary();
        
        $this->doRespondSuccess($result);
        
    }
    
    function getHelp() {
        
        $result = array();
        
        $result['help'] = $this->api_model->getHelp();
        
        $this->doRespondSuccess($result);
        
    }
    
    function blockMessage() {
        
        $result = array();
        
        $message_id = $_POST['messageId'];
        $message_content = $_POST['messageContent'];
        
        $this->api_model->updateMessage($message_id, $message_content);
        
        $this->doRespondSuccess($result);     
     
    }
    
    function getPreviousChallenges() {
        
        $result = array();
        $user_id = $_POST['userId'];
        
        $result['groupList'] = $this->api_model->getPreviousChallenges($user_id);
        
        $this->doRespondSuccess($result);
        
        
    }
    
                  
}

?>
