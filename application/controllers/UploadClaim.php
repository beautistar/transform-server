<?php
  
Class UploadClaim extends CI_Controller {
    
    function __construct(){

        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('api_model');
        $this->load->library('session');     
    }
    
    /**
     * Make json response to the client with result code message
     *
     * @param p_result_code : Result code
     * @param p_result_msg : Result message
     * @param p_result : Result json object
     */

    private function doRespond($p_result_code,  $p_result){

         $p_result['resultCode'] = $p_result_code;

         $this->output->set_content_type('application/json')->set_output(json_encode($p_result));
    }

    /**
     * Make json response to the client with success.
     * (result_code = 0, result_msg = "success")
     *
     * @param p_result : Result json object
     */

    private function doRespondSuccess($result){

         $this->doRespond(100, $result);
    }

    /**
     * Url decode.
     *
     * @param p_text : Data to decode
     *
     * @return text : Decoded text
     */

    private function doUrlDecode($p_text){

        $p_text = urldecode($p_text);
        $p_text = str_replace('&#40;', '(', $p_text);
        $p_text = str_replace('&#41;', ')', $p_text);
        $p_text = str_replace('%40', '@', $p_text);
        $p_text = str_replace('%20',' ', $p_text);
        $p_text = trim($p_text);


        return $p_text;
    }
    
    function index() {
         
         $result = array();
         
         $user_id = $_POST['user_id'];
         $file_type = $_POST['file_type'];
         $file_name = $_POST['file_name'];
         
         if ($file_type == "photo") {
             
             if(!is_dir("/claimfiles/photos/")) {
                mkdir("/claimfiles/photos/");             
             }
             $upload_path = "claimfiles/photos/";             
         } else {
             if(!is_dir("/Claimfiles/Videos/")) {
                mkdir("/Claimfiles/Videos/");             
             }
             $upload_path = "/Claimfiles/Videos/";
         }           

         $cur_time = time();
         
         $dateY = date("Y", $cur_time);
         $dateM = date("m", $cur_time);
         
         if(!is_dir($upload_path."/".$dateY)){
             mkdir($upload_path."/".$dateY);
         }
         if(!is_dir($upload_path."/".$dateY."/".$dateM)){
             mkdir($upload_path."/".$dateY."/".$dateM);
         }
         
         $upload_path .= $dateY."/".$dateM."/";
         $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 4000,
            'max_height' => 3000,
            'file_name' => $name.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('file')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $id = $this->api_model->registerFiles($user_id, $file_name, $file_type, $file_url);
            $result['file_url'] = $file_url;
            $this->doRespondSuccess($result);

        } else {

            $this->doRespond(206, $result);// upload fail
            return;
        }
    }
}
  
  
  
?>
